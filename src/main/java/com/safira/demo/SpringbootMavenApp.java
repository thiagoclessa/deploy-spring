package com.safira.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMavenApp {

  public static void main(String[] args) {
    SpringApplication.run(SpringbootMavenApp.class, args);
  }
}
